import React from "react";
import { Modal, Form, Input, Checkbox, Select, Radio } from "antd";
import { BASE_API_URL } from "./Settings";

const { Option } = Select;

const BinForm = Form.create({ name: "form_in_modal" })(
  class extends React.Component {
    state = {
      colors: [],
      categories: []
    };

    componentDidMount() {
      fetch(`${BASE_API_URL}api/category/all`)
        .then(response => response.json())
        .then(json => this.setState({ categories: json }));

      fetch(`${BASE_API_URL}api/color/all`)
        .then(response => response.json())
        .then(json => this.setState({ colors: json }));
    }

    handleCancel = () => {
      this.setState({ previewVisible: false });
      this.props.form.resetFields();
      this.setState({ category: null });
    };

    getArrayOfInitialValueIds() {
      const { bin } = this.props;
      let arrayOfInitialValueIds = [];

      if (bin && bin.categories) {
        const initialValue = bin.categories;

        arrayOfInitialValueIds = initialValue.map(value => value.id);
      }

      return arrayOfInitialValueIds;
    }

    getEmptyBin = () => ({
      id: null,
      name: "",
      categories: [
        {
          id: null,
          name: ""
        }
      ],
      color: {
        id: null,
        value: "",
        name: ""
      },
      type: "BIN"
    });

    getEditTexts = () => ({
      title: "Edytuj",
      okButton: "Zapisz",
      cancelButton: "Anuluj"
    });

    getCreateNewTexts = () => ({
      title: "Dodaj nowy pojemnik",
      okButton: "Dodaj",
      cancelButton: "Anuluj"
    });

    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { bin } = this.props;
      const { getFieldDecorator } = form;
      const { categories } = this.state;

      const arrayOfInitialValueIds = this.getArrayOfInitialValueIds();
      const currentBin =
        bin === null || typeof bin === "undefined" ? this.getEmptyBin() : bin;

      const options = categories.map(category => ({
        label: category.name,
        value: category.id
      }));

      const modalTexts =
        currentBin.id === null ? this.getCreateNewTexts() : this.getEditTexts();

      return (
        <Modal
          visible={visible}
          title={modalTexts.title}
          okText={modalTexts.okButton}
          cancelText={modalTexts.cancelButton}
          onCancel={() => {
            onCancel();
            form.resetFields();
          }}
          onOk={() => {
            onCreate();
            form.resetFields();
          }}
        >
          <Form layout="vertical">
            {getFieldDecorator("id", {
              initialValue: currentBin.id
            })(<Input type="hidden" />)}

            <Form.Item label="Nazwa">
              {getFieldDecorator("name", {
                rules: [
                  {
                    required: true,
                    message: "Wpisz nazwę pojemnika"
                  },
                  {
                    min: 3,
                    message: "Nazwa musi składać się z minimum 3 znaków"
                  }
                ],
                initialValue: currentBin.name
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Kolor">
              {getFieldDecorator("color", {
                rules: [
                  {
                    required: true,
                    type: "number",
                    message: "Wybierz kolor"
                  }
                ],
                initialValue: currentBin.color.id
              })(
                <Select
                  key={currentBin.color.id}
                  name="category"
                  showSearch
                  placeholder="Wybierz lub wyszukaj kolor"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {this.state.colors.map(color => (
                    <Option value={color.id} key={color.id}>
                      {color.name}
                    </Option>
                  ))}
                </Select>
              )}
            </Form.Item>

            <Form.Item label="Typ">
              {getFieldDecorator("type", {
                rules: [
                  {
                    required: true,
                    message: "Wybierz typ"
                  }
                ],
                initialValue: currentBin.type
              })(
                <Radio.Group key={currentBin.type} name="type">
                  <Radio value="BIN">Kubeł</Radio>
                  <Radio value="BAG">Worek</Radio>
                </Radio.Group>
              )}
            </Form.Item>

            <Form.Item label="Kategorie">
              {getFieldDecorator("categories", {
                rules: [
                  {
                    required: true,
                    type: "array",
                    message: "Wybierz co najmniej jedną kategorię"
                  }
                ],
                initialValue: arrayOfInitialValueIds
              })(
                <Checkbox.Group
                  key={arrayOfInitialValueIds}
                  options={options}
                />
              )}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  }
);

export default BinForm;
