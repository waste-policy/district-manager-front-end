import React from "react";
import Summary from "./Summary";
import Wastes from "./Wastes";
import Bins from "./Bins";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Menu, Icon, Layout, Typography } from "antd";
import ColorsSettings from "./ColorsSettings";

const { Header, Footer, Content, Sider } = Layout;
const { Title } = Typography;
const { SubMenu } = Menu;

class App extends React.Component {
  state = {
    current: "mail"
  };

  handleClick = e => {
    console.log("click ", e);
    this.setState({
      current: e.key
    });
  };

  render() {
    return (
      <div className="App">
        <Router>
          <Layout style={{ minHeight: "100vh" }}>
            <Sider>
              <Menu
                onClick={this.handleClick}
                selectedKeys={[this.state.current]}
                mode="inline"
                theme="dark"
                defaultSelectedKeys={["0"]}
                style={{ lineHeight: "64px", fontSize: "110%" }}
              >
                <Menu.Item key="dashboard">
                  <Link to="/">
                    <Icon type="dashboard" />
                    Podsumowanie
                  </Link>
                </Menu.Item>
                <Menu.Item key="incidents">
                  <Link to="/wastes">
                    <Icon type="warning" />
                    Incydenty
                  </Link>
                </Menu.Item>
                <SubMenu
                  key="calendar"
                  title={
                    <span>
                      <Icon type="calendar" />
                      <span>Terminarz</span>
                    </span>
                  }
                >
                  <Menu.Item key="wywozy">Wywozy</Menu.Item>
                  <Menu.Item key="zbiorki_smieci">Zbiórki śmieci</Menu.Item>
                </SubMenu>
                <Menu.Item key="bins">
                  <Link to="/bins">
                    <Icon type="rest" />
                    Pojemniki
                  </Link>
                </Menu.Item>
                <Menu.Item key="waste_points">
                  <Link to="/wastes">
                    <Icon type="environment" />
                    Punkty zbiórek
                  </Link>
                </Menu.Item>
                <Menu.Item key="wastes">
                  <Link to="/wastes">
                    <Icon type="barcode" />
                    Odpady
                  </Link>
                </Menu.Item>
                <Menu.Item key="game">
                  {/* <Link to="/wastes"> */}
                  <Icon type="laptop" />
                  Gra
                  {/* </Link> */}
                </Menu.Item>
                <SubMenu
                  key="settings"
                  title={
                    <span>
                      <Icon type="setting" />
                      <span>Ustawienia</span>
                    </span>
                  }
                >
                  <Menu.Item key="colors">
                    <Link to="/settings/colors">Kolory</Link>
                  </Menu.Item>
                  <Menu.Item key="settings/district_info">
                    Informacje o gminie
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Layout>
              <Header
                style={{
                  margin: "0px",
                  padding: " 16px 50px",
                  height: "auto",
                  background: "#fff"
                }}
              >
                <Title>
                  <Route path="/wastes">Odpady</Route>
                  <Route path="/bins">Pojemniki</Route>
                  <Route path="/settings/colors">Ustawienia kolorów</Route>
                  <Route path="/" exact>
                    Podsumowanie
                  </Route>
                </Title>
              </Header>
              <Content style={{ margin: "16px" }}>
                <div style={{ padding: "24px", background: "#fff" }}>
                  <Switch>
                    <Route path="/wastes">
                      <Wastes />
                    </Route>
                    <Route path="/settings/colors">
                      <ColorsSettings />
                    </Route>
                    <Route path="/bins">
                      <Bins />
                    </Route>
                    <Route path="/">
                      <Summary />
                    </Route>
                  </Switch>
                </div>
              </Content>
              <Footer style={{ textAlign: "center" }}>
                Copyright by{" "}
                <a href="https://krystiankomor.dev">krystiankomor.dev</a>
              </Footer>
            </Layout>
          </Layout>
        </Router>
      </div>
    );
  }
}

export default App;
