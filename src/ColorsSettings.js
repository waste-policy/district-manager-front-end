import React from "react";
import { BASE_API_URL } from "./Settings";
import { Table, Button, Typography, Modal, Divider } from "antd";
import ColorForm from "./ColorForm";
import { successMessage, errorMessage } from "./common";

const { Text } = Typography;

export default class ColorsSettings extends React.Component {
  state = {
    colors: [],
    colorModalVisible: false,
    color: null
  };

  componentDidMount() {
    this.getAllColorsData();
  }

  getAllColorsData() {
    fetch(`${BASE_API_URL}api/color/all`)
      .then(response => response.json())
      .then(json => this.setState({ colors: json }));
  }

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields(async (err, values) => {
      if (err) {
        return;
      }

      if (values.id) {
        const result = await this.makePutRequest(
          `${BASE_API_URL}api/color/${values.id}`,
          values
        );
        if (result.ok) successMessage("Pomyślnie zmodyfikowano");
        else errorMessage();
      } else {
        const result = await this.makePostRequest(
          `${BASE_API_URL}api/color`,
          values
        );
        if (result.ok) successMessage("Pomyślnie dodano");
        else errorMessage();
      }

      this.setColorModalVisible(false);
      this.getAllColorsData();
    });
  };

  makePostRequest(url, data) {
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
  }

  makePutRequest(url, data) {
    return fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
  }
  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  setColorModalVisible(colorModalVisible) {
    this.setState({ colorModalVisible });
  }

  showColorModal(color) {
    this.setState({ color });
    this.setColorModalVisible(true);
  }

  showDeleteConfirm({ id, name }) {
    const that = this;
    Modal.confirm({
      title: `Usunąć "${name}"?`,
      okText: "Usuń",
      cancelText: "Anuluj",
      okType: "danger",
      content:
        "Kolor nie będzie już dostępny. Nie będzie można z niego skorzystać w grze. Operacja jest nieodwracalna. Czy na pewno chcesz usunąć?",
      onOk() {
        return fetch(`${BASE_API_URL}api/color/${id}`, {
          method: "DELETE"
        })
          .then(response => {
            if (response.ok) {
              successMessage("Pomyślnie usunięto kolor");
              that.getAllBinsData();
            } else {
              errorMessage();
            }
          })
          .catch(() => console.log("Oops errors!"));
      },
      onCancel() {},
      centered: true
    });
  }

  render() {
    const { colors } = this.state;

    const columns = [
      {
        title: "Nazwa",
        dataIndex: "name",
        key: "name",
        align: "center",
        render: name => <Text strong>{name}</Text>
      },
      {
        title: "Wartość",
        dataIndex: "value",
        key: "value",
        align: "center"
      },
      {
        title: "Podgląd",
        dataIndex: "value",
        key: "id",
        align: "center",
        render: value => (
          <p>
            <img
              src={`${BASE_API_URL}api/generator/bin/${value}.svg`}
              height="50px"
              crossOrigin="anonymous"
              alt=""
            />
            <img
              src={`${BASE_API_URL}api/generator/bag/${value}.svg`}
              height="50px"
              crossOrigin="anonymous"
              alt=""
            />
          </p>
        )
      },
      {
        title: "Akcje",
        key: "action",
        align: "center",
        render: text => (
          <div>
            <Button type="primary" onClick={() => this.showColorModal(text)}>
              Edytuj
            </Button>{" "}
            <Button type="danger" onClick={() => this.showDeleteConfirm(text)}>
              Usuń
            </Button>
          </div>
        )
      }
    ];

    return (
      <div>
        <Button
          type="primary"
          icon="plus"
          shape="round"
          onClick={() => this.showColorModal(null)}
        >
          Dodaj nowy
        </Button>

        <Divider />

        <ColorForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.colorModalVisible}
          onCancel={() => {
            this.setColorModalVisible(false);
          }}
          onCreate={this.handleCreate}
          color={this.state.color}
        />

        <Table
          rowKey={color => color.id}
          dataSource={colors}
          columns={columns}
        />
      </div>
    );
  }
}
