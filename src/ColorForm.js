import React from "react";
import { Modal, Form, Input, Checkbox, Select, Radio } from "antd";
import { BASE_API_URL } from "./Settings";

const { Option } = Select;

const BinForm = Form.create({ name: "form_in_modal" })(
  class extends React.Component {
    state = {
      colors: [],
      categories: []
    };

    handleCancel = () => {
      this.props.form.resetFields();
      this.setState({ category: null });
    };

    getEmptyColor = () => ({
      id: null,
      name: "",
      value: "ffffff"
    });

    getEditTexts = () => ({
      title: "Edytuj",
      okButton: "Zapisz",
      cancelButton: "Anuluj"
    });

    getCreateNewTexts = () => ({
      title: "Dodaj nowy kolor",
      okButton: "Dodaj",
      cancelButton: "Anuluj"
    });

    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { color } = this.props;
      const { getFieldDecorator } = form;

      const currentColor =
        color === null || typeof color === "undefined"
          ? this.getEmptyColor()
          : color;

      const modalTexts =
      currentColor.id === null ? this.getCreateNewTexts() : this.getEditTexts();

      return (
        <Modal
          visible={visible}
          title={modalTexts.title}
          okText={modalTexts.okButton}
          cancelText={modalTexts.cancelButton}
          onCancel={() => {
            onCancel();
            form.resetFields();
          }}
          onOk={() => {
            onCreate();
            form.resetFields();
          }}
        >
          <Form layout="vertical">
            {getFieldDecorator("id", {
              initialValue: currentColor.id
            })(<Input type="hidden" />)}

            <Form.Item label="Nazwa">
              {getFieldDecorator("name", {
                rules: [
                  {
                    required: true,
                    message: "Wpisz nazwę coloru"
                  },
                  {
                    min: 3,
                    message: "Nazwa musi składać się z minimum 3 znaków"
                  }
                ],
                initialValue: currentColor.name
              })(<Input />)}
            </Form.Item>

            <Form.Item label="Wartość">
              {getFieldDecorator("value", {
                rules: [
                  {
                    required: true,
                    message: "Wybierz kolor"
                  }
                ],
                initialValue: currentColor.value
              })(
                <Input />
              )}
            </Form.Item>

          </Form>
        </Modal>
      );
    }
  }
);

export default BinForm;
