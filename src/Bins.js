import React from "react";
import { BASE_API_URL } from "./Settings";
import { Table, Button, Typography, Modal, Divider } from "antd";
import BinForm from "./BinForm";
import { successMessage, errorMessage } from "./common";

const { Text } = Typography;

export default class Wastes extends React.Component {
  state = {
    wasteCollectors: [],
    binModalVisible: false,
    bin: null
  };

  componentDidMount() {
    this.getAllBinsData();
  }

  getAllBinsData() {
    fetch(`${BASE_API_URL}api/collector/17`)
      .then(response => response.json())
      .then(json => this.setState({ wasteCollectors: json }));
  }

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields(async (err, values) => {
      if (err) {
        return;
      }

      const returnObject = {
        id: values.id,
        name: values.name,
        color: {
          id: values.color
        },
        type: values.type,
        categories: values.categories.map(category => ({
          id: category
        })),
        district: {
          id: 17
        }
      };

      if (returnObject.id) {
        const result = await this.makePutRequest(
          `${BASE_API_URL}api/collector/${returnObject.id}`,
          returnObject
        );
        if (result.ok) successMessage("Pomyślnie zmodyfikowano");
        else errorMessage();
      } else {
        const result = await this.makePostRequest(
          `${BASE_API_URL}api/collector`,
          returnObject
        );
        if (result.ok) successMessage("Pomyślnie dodano");
        else errorMessage();
      }

      this.setBinModalVisible(false);
      this.getAllBinsData();
    });
  };

  makePostRequest(url, data) {
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
  }

  makePutRequest(url, data) {
    return fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
  }
  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  setBinModalVisible(binModalVisible) {
    this.setState({ binModalVisible });
  }

  showBinModal(bin) {
    this.setState({ bin });
    this.setBinModalVisible(true);
  }

  showDeleteConfirm({ id, name }) {
    const that = this;
    Modal.confirm({
      title: `Usunąć "${name}"?`,
      okText: "Usuń",
      cancelText: "Anuluj",
      okType: 'danger',
      content:
        "Pojemnik nie będzie już dostępny. Nie będzie można z niego skorzystać w grze. Nie będzie można go wyszukać. Operacja jest nieodwracalna. Czy na pewno chcesz usunąć?",
      onOk() {
        return fetch(`${BASE_API_URL}api/collector/${id}`, {
          method: "DELETE"
        })
          .then(response => {
            if (response.ok) {
              successMessage("Pomyślnie usunięto pojemnik");
              that.getAllBinsData();
            } else {
              errorMessage();
            }
          })
          .catch(() => console.log("Oops errors!"));
      },
      onCancel() {},
      centered: true
    });
  }

  render() {
    const columns = [
      {
        title: "Nazwa",
        dataIndex: "name",
        key: "name",
        render: name => <Text strong>{name}</Text>
      },
      {
        title: "Kolor",
        dataIndex: "color.name",
        key: "color.id"
      },
      {
        title: "Kategorie",
        dataIndex: "categories",
        key: "categories",
        render: categories => (
          <ul>
            {categories.map(category => (
              <li key={category.id}>{category.name}</li>
            ))}
          </ul>
        )
      },
      {
        title: "Typ",
        dataIndex: "type",
        key: "type"
      },
      {
        title: "Akcje",
        key: "action",
        render: text => (
          <div>
            <Button type="primary" onClick={() => this.showBinModal(text)}>
              Edytuj
            </Button>{" "}
            <Button type="danger" onClick={() => this.showDeleteConfirm(text)}>
              Usuń
            </Button>
          </div>
        )
      }
    ];

    const { wasteCollectors } = this.state;
    return (
      <div>
        <Button
          type="primary"
          icon="plus"
          shape="round"
          onClick={() => this.showBinModal(null)}
        >
          Dodaj nowy
        </Button>

        <Divider />

        <BinForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.binModalVisible}
          onCancel={() => {
            this.setBinModalVisible(false);
          }}
          onCreate={this.handleCreate}
          bin={this.state.bin}
        />

        <Table
          rowKey={bin => bin.id}
          dataSource={wasteCollectors}
          columns={columns}
        />
      </div>
    );
  }
}
