import React from "react";
import { BASE_API_URL } from "./Settings";
import { Table, Button, Typography, Modal, Divider } from "antd";
import WasteForm from "./WasteForm";
import { successMessage, errorMessage } from "./common"

const { Text } = Typography;

export default class Wastes extends React.Component {
  state = {
    wastes: [],
    wasteModalVisible: false,
    waste: null
  };

  componentDidMount() {
    this.getAllWasteData();
  }

  getAllWasteData() {
    fetch(`${BASE_API_URL}api/waste/all`)
      .then(response => response.json())
      .then(json => this.setState({ wastes: json }));
  }

  handleCreate = images => {
    const { form } = this.formRef.props;
    form.validateFields(async (err, values) => {
      if (err) {
        return;
      }

      const image = () => {
        if (values.image) return { id: values.image.toString() };

        if (images && images[0]) return { id: images[0].uid.toString() };

        return null;
      };

      const returnObject = {
        id: values.id,
        name: values.name,
        category: {
          id: values.category.toString()
        },
        image: image()
      };

      if (returnObject.id) {
        const result = await this.makePutRequest(
          `${BASE_API_URL}api/waste/${returnObject.id}`,
          returnObject
        );
        if (result.ok) successMessage("Pomyślnie zmodyfikowano");
        else errorMessage();
      } else {
        const result = await this.makePostRequest(
          `${BASE_API_URL}api/waste`,
          returnObject
        );
        if (result.ok) successMessage("Pomyślnie dodano");
        else this.errorMessage();
      }

      this.setWasteModalVisible(false);
      this.getAllWasteData();
    });
  };

  makePostRequest(url, data) {
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
  }

  makePutRequest(url, data) {
    return fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
  }

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  setWasteModalVisible(wasteModalVisible) {
    this.setState({ wasteModalVisible });
  }

  showWasteModal(waste) {
    this.setState({ waste });
    this.setWasteModalVisible(true);
  }

  showDeleteConfirm({ id, name }) {
    const that = this;
    Modal.confirm({
      title: `Usunąć "${name}"?`,
      okText: "Usuń",
      cancelText: "Anuluj",
      okType: 'danger',
      content:
        "Odpad nie będzie już dostępny. Nie będzie można z niego skorzystać w grze. Nie będzie można go wyszukać. Operacja jest nieodwracalna. Czy na pewno chcesz usunąć?",
      onOk() {
        return fetch(`${BASE_API_URL}api/waste/${id}`, {
          method: "DELETE"
        })
          .then(response =>
            response.ok ? that.getAllWasteData() : console.warn(response)
          )
          .catch(() => console.log("Oops errors!"));
      },
      onCancel() {},
      centered: true
    });
  }

  render() {
    const columns = [
      {
        title: "Zdjęcie",
        dataIndex: "image",
        key: "image.id",
        align: "center",
        render: image => {
          if (image && image.fileName)
            return <img src={image.fileName} height="60px" alt="" />;
        }
      },
      {
        title: "Nazwa",
        dataIndex: "name",
        key: "name",
        align: "center",
        render: name => <Text strong>{name}</Text>
      },
      {
        title: "Kategoria",
        dataIndex: "category.name",
        key: "category.id",
        align: "center"
      },
      {
        title: "Akcje",
        key: "action",
        align: "center",
        render: (text, record) => (
          <div>
            <Button type="primary" onClick={() => this.showWasteModal(record)}>
              Edytuj
            </Button>{" "}
            <Button
              type="danger"
              onClick={() => this.showDeleteConfirm(record)}
            >
              Usuń
            </Button>
          </div>
        )
      }
    ];

    const { wastes } = this.state;
    return (
      <div>
        <Button
          type="primary"
          icon="plus"
          shape="round"
          onClick={() => this.showWasteModal()}
        >
          Dodaj nowy
        </Button>

        <Divider />

        <WasteForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.wasteModalVisible}
          onCancel={() => {
            this.setWasteModalVisible(false);
          }}
          onCreate={this.handleCreate}
          waste={this.state.waste}
        />

        <Table
          rowKey={waste => waste.id}
          dataSource={wastes}
          columns={columns}
        />
      </div>
    );
  }
}
