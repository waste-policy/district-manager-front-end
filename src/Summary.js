import React from "react";
import { BASE_API_URL } from "./Settings";
import { Row, Col } from "antd";

class Summary extends React.Component {
  state = {
    wastes: [],
    wasteCollectors: [],
    colors: [],
    stages: [],
    isLoadingWastes: true,
    isLoadingWasteCollectors: true,
    isLoadingStages: true,
    isLoadingColors: true
  };

  componentDidMount() {
    fetch(`${BASE_API_URL}api/waste/all`)
      .then(response => response.json())
      .then(json => this.setState({ wastes: json, isLoadingWastes: false }));

    fetch(`${BASE_API_URL}api/wasteCollector/all`)
      .then(response => response.json())
      .then(json =>
        this.setState({
          wasteCollectors: json,
          isLoadingWasteCollectors: false
        })
      );

    fetch(`${BASE_API_URL}api/stage/all`)
      .then(response => response.json())
      .then(json => this.setState({ stages: json, isLoadingStages: false }));

    fetch(`${BASE_API_URL}api/color/all`)
      .then(response => response.json())
      .then(json => this.setState({ colors: json, isLoadingColors: false }));
  }

  render() {
    const {
      wastes,
      wasteCollectors,
      stages,
      colors,
      isLoadingWastes,
      isLoadingWasteCollectors,
      isLoadingStages,
      isLoadingColors
    } = this.state;

    if (
      isLoadingWastes &&
      isLoadingWasteCollectors &&
      isLoadingStages &&
      isLoadingColors
    ) {
      return <h1>Proszę czekać</h1>;
    }

    return (
      <div>
        <h1>Podsumowanie</h1>
        <Row>
          <Col xs={{ span: 5, offset: 1 }} lg={{ span: 5, offset: 1 }}>
            <h2>Śmieci</h2>
            <ul>
              {wastes.map(waste => (
                <li key={waste.id}>{waste.name}</li>
              ))}
            </ul>
          </Col>
          <Col xs={{ span: 11, offset: 1 }} lg={{ span: 5, offset: 1 }}>
            <h2>Pojemniki</h2>
            <ul>
              {wasteCollectors.map(wasteCollector => (
                <li key={wasteCollector.id}>{wasteCollector.name}</li>
              ))}
            </ul>
          </Col>
          <Col xs={{ span: 11, offset: 1 }} lg={{ span: 5, offset: 1 }}>
            <h2>Poziomy</h2>
            <ul>
              <li>{stages.length}</li>
            </ul>
          </Col>
          <Col xs={{ span: 11, offset: 1 }} lg={{ span: 5, offset: 1 }}>
            <h2>Kolory</h2>
            <ul>
              {colors.map(color => (
                <li key={color.id}>
                  {color.name} (#{color.value})
                </li>
              ))}
            </ul>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Summary;
