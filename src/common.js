import { message } from "antd";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

function successMessage(msg) {
  message.success(msg);
}

function errorMessage(msg) {
  message.error(msg || "Wystąpił błąd");
}

export { getBase64, successMessage, errorMessage };
