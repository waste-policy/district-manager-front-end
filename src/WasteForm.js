import React from "react";
import { getBase64 } from "./common";
import { Upload, Icon, Modal, Form, Input, Select } from "antd";
import { BASE_API_URL } from "./Settings";
const { Option } = Select;

const WasteForm = Form.create({ name: "form_in_modal" })(
  class extends React.Component {
    state = {
      previewVisible: false,
      previewImage: "",
      fileList: [],
      category: null,
      categories: []
    };

    componentDidMount() {
      fetch(`${BASE_API_URL}api/category/all`)
        .then(response => response.json())
        .then(json => this.setState({ categories: json }));
    }

    handleCancel = () => {
      this.setState({ previewVisible: false });
      this.props.form.resetFields();
      this.setState({ category: null });
    };

    getImages() {
      const { waste } = this.props;
      if (
        waste &&
        waste.image &&
        waste.image.fileName &&
        this.state.fileList.length === 0
      ) {
        return [
          {
            uid: waste.image.id,
            name: waste.image.fileName,
            status: "done",
            url: waste.image.fileName
          }
        ];
      }

      return this.state.fileList;
    }

    handlePreview = async file => {
      if (!file.url && !file.preview) {
        file.preview = await getBase64(file.originFileObj);
      }

      this.setState({
        previewImage: file.url || file.preview,
        previewVisible: true
      });
    };

    handleChange = ({ fileList }) => this.setState({ fileList });

    getEmptyWaste = () => ({
      id: null,
      name: "",
      category: {
        id: null,
        name: ""
      }
    });

    getUploadButton = () => (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Wgraj obraz</div>
      </div>
    );

    getEditTexts = () => ({
      title: "Edytuj",
      okButton: "Zapisz",
      cancelButton: "Anuluj"
    });

    getCreateNewTexts = () => ({
      title: "Dodaj nowy odpad",
      okButton: "Dodaj",
      cancelButton: "Anuluj"
    });

    render() {
      const { visible, onCancel, onCreate, form, waste } = this.props;
      const { getFieldDecorator } = form;
      const { previewVisible, previewImage, categories } = this.state;

      const currentWaste =
        waste === null || typeof waste === "undefined"
          ? this.getEmptyWaste()
          : waste;

      const modalTexts =
        currentWaste.id === null
          ? this.getCreateNewTexts()
          : this.getEditTexts();

      return (
        <Modal
          visible={visible}
          title={modalTexts.title}
          okText={modalTexts.okButton}
          cancelText={modalTexts.cancelButton}
          onCancel={() => {
            onCancel();
            form.resetFields();
          }}
          onOk={() => {
            onCreate(this.getImages());
            form.resetFields();
          }}
        >
          <Form layout="vertical">
            {getFieldDecorator("id", {
              initialValue: currentWaste.id
            })(<Input type="hidden" />)}
            <Form.Item label="Nazwa">
              {getFieldDecorator("name", {
                rules: [
                  {
                    required: true,
                    message: "Nazwa odpadu jest wymagana"
                  }
                ],
                initialValue: currentWaste.name
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Kategoria">
              {getFieldDecorator("category", {
                rules: [
                  {
                    required: true,
                    type: "number"
                  }
                ],
                initialValue: currentWaste.category.id
              })(
                <Select
                  key={currentWaste}
                  name="category"
                  showSearch
                  placeholder="Wybierz lub wyszukaj kategorię"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {categories.map(category => (
                    <Option value={category.id} key={category.id}>
                      {category.name}
                    </Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label="Obraz">
              <div className="clearfix">
                <Upload
                  action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                  listType="picture-card"
                  fileList={this.getImages()}
                  onPreview={this.handlePreview}
                  onChange={this.handleChange}
                >
                  {this.getImages().length >= 1 ? null : this.getUploadButton()}
                </Upload>
                <Modal
                  visible={previewVisible}
                  footer={null}
                  onCancel={this.handleCancel}
                  style={{ textAlign: "center" }}
                >
                  <img alt="example" src={previewImage} />
                </Modal>
              </div>
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  }
);

export default WasteForm;
